<?php
namespace app;

use \DOMDocument;
use \DOMXPath;
use \app\GenerationModel;

class ParserDrtive2
{
    const path = 'https://www.drive2.ru';
    protected $cUrl;
    private $brands = array();
    private $models = array();
    private $generations = array();
    const brandClass = 'c-makes__item';

    public function __construct()
    {
        /*getting html page*/
        $ch = curl_init(self::path . '/cars/?all');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $this->cUrl = curl_exec($ch);
        curl_close($ch);
    }

    public function getBrands()
    {
        $finder = $this->createXPath($this->cUrl);
        $nodes = $finder->query("//*[contains(@class, '" . self::brandClass . "')]");
        foreach ($nodes as $node) {
            $this->brands[$node->nodeValue] = $node->firstChild->getAttribute('href');
        }
        return $this->brands;
    }

    public function getModels()
    {
        if (empty($this->models)) {
            if (empty($this->brands))
                $this->getBrands();
            $modelsPages = $this->multiCurl($this->brands);
            foreach ($modelsPages as $i => $modelPge) {
                $finder = $this->createXPath($modelPge);
                $nodes = $finder->query("//span[contains(@class, 'c-makes__item is-important js-makes-item')]");
                if ($nodes->length > 0) {
                    foreach ($nodes as $node) {
                        $this->models[$i][] = [$node->firstChild->nodeValue => $node->firstChild->getAttribute('href')];
                    }
                }
            }
            return $this->models;
        } else
            return $this->models;

    }

    public function getGenerations()
    {
        if (empty($this->generations)) {
            if (empty($this->brands))
                $this->getBrands();
            if (empty($this->models))
                $this->getModels();
            $genPages = array();
            foreach ($this->models as $key => $model) {

                $genPages[$key] = $this->multiCurl($model);
            }
            foreach ($genPages as $key => $genPage) {
                foreach ($genPage as $model => $gene) {
                    $genModel = new GenerationModel();
                    $finder = $this->createXPath($gene);
                    $nodes = $finder->query("//div[@class = 'o-grid__item is-important js-makes-item']/div[@class = 'c-gen-card']");
                    if ($nodes->length > 0) {
                        foreach ($nodes as $node) {
                            if ($node->nodeType === 1) {

                                $genModel->link = $node->getElementsByTagName('a')[1]->getAttribute('href');
                                $genModel->name = $node->getElementsByTagName('a')[1]->nodeValue;
                                $genModel->image = $node->getElementsByTagName('img')[0]->getAttribute('src');
                                $genModel->modelName = $model;
                                $this->generations[$key][] = $genModel;
                            }

                        }
                    }

                }

            }
            return $this->generations;
        } else
            return $this->generations;
    }

    public function multiCurl(array $urls)
    {
        $mh = curl_multi_init();
        foreach ($urls as $i => $url) {
            if (is_array($url)) {
                foreach ($url as $key => $value) {
                    $conn[$key] = curl_init('https://www.drive2.ru' . $value);
                    curl_setopt($conn[$key], CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($conn[$key], CURLOPT_CONNECTTIMEOUT, 0);
                    curl_multi_add_handle($mh, $conn[$key]);
                }
            } else {
                $conn[$i] = curl_init('https://www.drive2.ru' . $url);
                curl_setopt($conn[$i], CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($conn[$i], CURLOPT_CONNECTTIMEOUT, 0);
                curl_multi_add_handle($mh, $conn[$i]);
            }

        }
        $active = null;
        do {
            curl_multi_exec($mh, $active);
        } while ($active);
        foreach ($urls as $i => $url) {
            if (is_array($url)) {
                foreach ($url as $key => $value) {
                    //ответ сервера в переменную
                    $res[$key] = curl_multi_getcontent($conn[$key]);
                    curl_multi_remove_handle($mh, $conn[$key]);
                    curl_close($conn[$key]);
                }
            } else {
                //ответ сервера в переменную
                $res[$i] = curl_multi_getcontent($conn[$i]);
                curl_multi_remove_handle($mh, $conn[$i]);
                curl_close($conn[$i]);
            }

        }
        curl_multi_close($mh);
        return $res;
    }


    public function createXPath($page)
    {
        $dom = new DOMDocument();
        $internalErrors = libxml_use_internal_errors(true);
        $dom->loadHTML($page);
        libxml_use_internal_errors($internalErrors);
        return new DOMXPath($dom);
    }


}