<?php
namespace app;
use \PDO;
use \PDOException;

class Repository
{
    /**
     * @var PDO $pdo
     */
    private $pdo = null;
    private $db = array('host'=>'localhost','dbname'=>'parser', 'user'=>'root', 'password'=>'');

    public function __construct($arrayDbSettings = null)
    {
        $this->connect($arrayDbSettings = null);
        if($arrayDbSettings !== null)
            $this->db = $arrayDbSettings;

    }

    public function insertBrands(array $brands){
        if(is_array($brands)){
            if($this->pdo === null){
                $this->connect($this->db);
            }
            $flag = true;
            $stmt = $this->pdo->prepare("INSERT INTO brands (name, link) VALUES (:name, :link)");
            $stmt->bindParam(':name',$name);
            $stmt->bindParam(':link',$link);
            foreach ($brands as $key => $value){
                $name = $key;
                $link = $value;
                $flag = $stmt->execute() ? true : false;

            }
            if($flag === true)
                return true;
            else
                return false;

            
        }
        else
            throw new Exception ('Argument is not array for insertBrands');
    }
    public function insertModels(array $models){
        if(is_array($models)){
            if($this->pdo === null){
                $this->connect($this->db);
            }
            $stmt = $this->pdo->prepare("INSERT INTO models(name,link,brandId) VALUES (:name,:link, (SELECT id FROM brands WHERE name = :brandId))");
            $stmt->bindParam(':name',$name);
            $stmt->bindParam(':link', $link);
            $stmt->bindParam(':brandId', $brandId);
            $flag = true;
            foreach ($models as $key => $model){
                $brandId = $key;
                foreach ($model as $item => $value){
                    $name = array_keys($value)[0];
                    $link = $value[$name];
                    $flag = $stmt->execute() ? true : false;
                }
                
            }
            if($flag === true)
                return true;
            else
                return false;
        }
        else
            throw new Exception ('Argument is not array for insertModels');

    }
    
    public function insertGenerations(array $generations){
        //toDo implementation 
    }

    public function connect($arrayDbSettings = null){
        if($this->pdo === null){
            if($arrayDbSettings === null)
                try{
                    $this->pdo = new PDO("mysql:host=".$this->db['host'].";dbname=".$this->db['dbname'],$this->db['user'],$this->db['password']);
                }
                catch(PDOException $ex){
                    echo $ex->getMessage();
                }

            else
                try{
                    $this->pdo = new PDO("mysql:host=".$arrayDbSettings['host'].";dbname=".$arrayDbSettings['dbname'],$arrayDbSettings['user'],$arrayDbSettings['password']);
                }
                catch(PDOException $ex){
                    echo $ex->getMessage();
                }

        }
    }

    public function close(){
        $this->pdo = null;
    }

}