-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: parser
-- ------------------------------------------------------
-- Server version	5.7.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `link` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1225 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brands`
--

LOCK TABLES `brands` WRITE;
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
INSERT INTO `brands` VALUES (1075,'527','/r/527/'),(1076,'AC Cobra','/r/ac/'),(1077,'Acura','/r/acura/'),(1078,'Alfa Romeo','/r/alfaromeo/'),(1079,'AMC','/r/amc/'),(1080,'ARO','/r/aro/'),(1081,'Asia','/r/asia/'),(1082,'Aston Martin','/r/astonmartin/'),(1083,'Audi','/r/audi/'),(1084,'Austin','/r/austin/'),(1085,'Autozam','/r/autozam/'),(1086,'Avia','/r/avia/'),(1087,'BAC','/r/bac/'),(1088,'Barkas','/r/barkas/'),(1089,'BCC','/r/bcc/'),(1090,'Bentley','/r/bentley/'),(1091,'Bertone','/r/bertone/'),(1092,'BMW','/r/bmw/'),(1093,'Bogdan','/r/bogdan/'),(1094,'Brilliance','/r/brilliance/'),(1095,'Bugatti','/r/bugatti/'),(1096,'Buick','/r/buick/'),(1097,'BYD','/r/byd/'),(1098,'Cadillac','/r/cadillac/'),(1099,'Caterham','/r/caterham/'),(1100,'Changan','/r/changan/'),(1101,'Chery','/r/chery/'),(1102,'Chevrolet','/r/chevrolet/'),(1103,'Chrysler','/r/chrysler/'),(1104,'Citroen','/r/citroen/'),(1105,'Cord','/r/cord/'),(1106,'Dacia','/r/dacia/'),(1107,'Daewoo','/r/daewoo/'),(1108,'Daihatsu','/r/daihatsu/'),(1109,'Daimler','/r/daimler/'),(1110,'Datsun','/r/datsun/'),(1111,'de Tomaso','/r/detomaso/'),(1112,'DeLorean','/r/delorean/'),(1113,'Derways','/r/derways/'),(1114,'Dodge','/r/dodge/'),(1115,'Dongfeng','/r/dongfeng/'),(1116,'Eagle','/r/eagle/'),(1117,'Efini','/r/efini/'),(1118,'Eunos','/r/eunos/'),(1119,'FAW','/r/faw/'),(1120,'Ferrari','/r/ferrari/'),(1121,'FIAT','/r/fiat/'),(1122,'Ford','/r/ford/'),(1123,'Foton','/r/foton/'),(1124,'FSO','/r/fso/'),(1125,'Geely','/r/geely/'),(1126,'Genesis','/r/genesis/'),(1127,'GMC','/r/gmc/'),(1128,'Great Wall','/r/greatwall/'),(1129,'Hafei','/r/hafei/'),(1130,'Haima','/r/haima/'),(1131,'Haval','/r/haval/'),(1132,'Hawtai','/r/hawtai/'),(1133,'Holden','/r/holden/'),(1134,'Honda','/r/honda/'),(1135,'Hummer','/r/hummer/'),(1136,'Hyundai','/r/hyundai/'),(1137,'Infiniti','/r/infiniti/'),(1138,'Iran Khodro','/r/ikco/'),(1139,'Isuzu','/r/isuzu/'),(1140,'Iveco','/r/iveco/'),(1141,'JAC','/r/jac/'),(1142,'Jaguar','/r/jaguar/'),(1143,'Jeep','/r/jeep/'),(1144,'KIA','/r/kia/'),(1145,'KTM','/r/ktm/'),(1146,'Lamborghini','/r/lamborghini/'),(1147,'Lancia','/r/lancia/'),(1148,'Land Rover','/r/landrover/'),(1149,'LDV','/r/ldv/'),(1150,'Lexus','/r/lexus/'),(1151,'Lifan','/r/lifan/'),(1152,'Ligier','/r/ligier/'),(1153,'Lincoln','/r/lincoln/'),(1154,'Lotus','/r/lotus/'),(1155,'Luxgen','/r/luxgen/'),(1156,'Marussia','/r/marussia/'),(1157,'Maserati','/r/maserati/'),(1158,'Maybach','/r/maybach/'),(1159,'Mazda','/r/mazda/'),(1160,'Mercedes-Benz','/r/mercedes/'),(1161,'Mercury','/r/mercury/'),(1162,'Merkur','/r/merkur/'),(1163,'MG','/r/mg/'),(1164,'MINI','/r/mini/'),(1165,'Mitsubishi','/r/mitsubishi/'),(1166,'Morgan','/r/morgan/'),(1167,'Morris','/r/morris/'),(1168,'Nissan','/r/nissan/'),(1169,'Noble','/r/noble/'),(1170,'Nysa','/r/nysa/'),(1171,'Oldsmobile','/r/oldsmobile/'),(1172,'Opel','/r/opel/'),(1173,'Peugeot','/r/peugeot/'),(1174,'Plymouth','/r/plymouth/'),(1175,'Pontiac','/r/pontiac/'),(1176,'Porsche','/r/porsche/'),(1177,'Proton','/r/proton/'),(1178,'Ravon','/r/ravon/'),(1179,'Renault','/r/renault/'),(1180,'Rolls-Royce','/r/rollsroyce/'),(1181,'Rover','/r/rover/'),(1182,'Saab','/r/saab/'),(1183,'Samsung','/r/samsung/'),(1184,'Saturn','/r/saturn/'),(1185,'Scion','/r/scion/'),(1186,'SEAT','/r/seat/'),(1187,'Simca','/r/simca/'),(1188,'Skoda','/r/skoda/'),(1189,'SMA','/r/sma/'),(1190,'Smart','/r/smart/'),(1191,'SsangYong','/r/ssangyong/'),(1192,'Subaru','/r/subaru/'),(1193,'Suzuki','/r/suzuki/'),(1194,'Talbot','/r/talbot/'),(1195,'Tata','/r/tata/'),(1196,'Tatra','/r/tatra/'),(1197,'Tesla','/r/tesla/'),(1198,'Toyota','/r/toyota/'),(1199,'Trabant','/r/trabant/'),(1200,'Vauxhall','/r/vauxhall/'),(1201,'Volkswagen','/r/volkswagen/'),(1202,'Volvo','/r/volvo/'),(1203,'Wartburg','/r/wartburg/'),(1204,'Wiesmann','/r/wiesmann/'),(1205,'Willys','/r/willys/'),(1206,'Zotye','/r/zotye/'),(1207,'ZX','/r/zx/'),(1208,'Автокам','/r/avtokam/'),(1209,'ВИС','/r/vis/'),(1210,'ГАЗ','/r/gaz/'),(1211,'ЕрАЗ','/r/eraz/'),(1212,'ЗАЗ','/r/zaz/'),(1213,'ЗИЛ','/r/zil/'),(1214,'ИЖ','/r/izh/'),(1215,'КАМАЗ','/r/kamaz/'),(1216,'Лада','/r/lada/'),(1217,'ЛуАЗ','/r/luaz/'),(1218,'Москвич','/r/moskvich/'),(1219,'РАФ','/r/raf/'),(1220,'СеАЗ','/r/seaz/'),(1221,'ТагАЗ','/r/tagaz/'),(1222,'УАЗ','/r/uaz/'),(1223,'Другие марки','/r/other/'),(1224,'Самоделки','/r/selfmade/');
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `generations`
--

DROP TABLE IF EXISTS `generations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `generations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `link` varchar(100) NOT NULL,
  `modelId` int(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `modelForGeneration_idx` (`modelId`),
  CONSTRAINT `modelForGeneration` FOREIGN KEY (`modelId`) REFERENCES `models` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `generations`
--

LOCK TABLES `generations` WRITE;
/*!40000 ALTER TABLE `generations` DISABLE KEYS */;
/*!40000 ALTER TABLE `generations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `models`
--

DROP TABLE IF EXISTS `models`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `models` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `link` varchar(100) NOT NULL,
  `brandId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `brandForModel_idx` (`brandId`),
  CONSTRAINT `brandForModel` FOREIGN KEY (`brandId`) REFERENCES `brands` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=297 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `models`
--

LOCK TABLES `models` WRITE;
/*!40000 ALTER TABLE `models` DISABLE KEYS */;
INSERT INTO `models` VALUES (157,'CL','/r/acura/m6/',1077),(158,'CSX','/r/acura/m13/',1077),(159,'EL','/r/acura/m5/',1077),(160,'ILX','/r/acura/m2637/',1077),(161,'ILX Hybrid','/r/acura/m2642/',1077),(162,'Integra','/r/acura/m1/',1077),(163,'Integra Type R','/r/acura/m2/',1077),(164,'Legend','/r/acura/m4/',1077),(165,'MDX','/r/acura/m14/',1077),(166,'NSX','/r/acura/m8/',1077),(167,'RDX','/r/acura/m15/',1077),(168,'RL','/r/acura/m10/',1077),(169,'RLX','/r/acura/m2644/',1077),(170,'RSX','/r/acura/m3/',1077),(171,'TL','/r/acura/m11/',1077),(172,'TLX','/r/acura/m2643/',1077),(173,'TSX','/r/acura/m12/',1077),(174,'Vigor','/r/acura/m7/',1077),(175,'ZDX','/r/acura/m1968/',1077),(176,'145','/r/alfaromeo/m242/',1078),(177,'146','/r/alfaromeo/m243/',1078),(178,'147','/r/alfaromeo/m247/',1078),(179,'147 GTA','/r/alfaromeo/m248/',1078),(180,'155','/r/alfaromeo/m238/',1078),(181,'156','/r/alfaromeo/m244/',1078),(182,'156 GTA','/r/alfaromeo/m245/',1078),(183,'156 Sportwagon','/r/alfaromeo/m2654/',1078),(184,'159','/r/alfaromeo/m251/',1078),(185,'159 Sportwagon','/r/alfaromeo/m2656/',1078),(186,'164','/r/alfaromeo/m239/',1078),(187,'166','/r/alfaromeo/m249/',1078),(188,'33','/r/alfaromeo/m236/',1078),(189,'4C','/r/alfaromeo/m2652/',1078),(190,'75','/r/alfaromeo/m237/',1078),(191,'90','/r/alfaromeo/m234/',1078),(192,'Alfasud','/r/alfaromeo/m228/',1078),(193,'Alfasud Sprint','/r/alfaromeo/m229/',1078),(194,'Alfetta','/r/alfaromeo/m226/',1078),(195,'Alfetta Coupe','/r/alfaromeo/m227/',1078),(196,'Arna','/r/alfaromeo/m235/',1078),(197,'Brera','/r/alfaromeo/m252/',1078),(198,'Crosswagon Q4','/r/alfaromeo/m2655/',1078),(199,'Giulia Sprint','/r/alfaromeo/m2552/',1078),(200,'Giulietta','/r/alfaromeo/m230/',1078),(201,'GT','/r/alfaromeo/m250/',1078),(202,'GTV','/r/alfaromeo/m246/',1078),(203,'Mi.To','/r/alfaromeo/m1560/',1078),(204,'RZ','/r/alfaromeo/m241/',1078),(205,'Spider','/r/alfaromeo/m232/',1078),(206,'SZ','/r/alfaromeo/m240/',1078),(207,'Eagle','/r/amc/m2737/',1079),(208,'Gremlin','/r/amc/m2876/',1079),(209,'Javelin','/r/amc/m2742/',1079),(210,'10','/r/aro/m2457/',1080),(211,'24/32','/r/aro/m2458/',1080),(212,'IMS','/r/aro/m2456/',1080),(213,'Rocsta','/r/asia/m2921/',1081),(214,'Rocsta R2','/r/asia/m2922/',1081),(215,'DB7 V12 Vantage','/r/astonmartin/m506/',1082),(216,'DB9','/r/astonmartin/m511/',1082),(217,'DBS','/r/astonmartin/m514/',1082),(218,'Rapide','/r/astonmartin/m256/',1082),(219,'V8 Vantage Roadster','/r/astonmartin/m513/',1082),(220,'Virage','/r/astonmartin/m499/',1082),(221,'100','/r/audi/m37/',1083),(222,'100 Avant','/r/audi/m3040/',1083),(223,'200','/r/audi/m38/',1083),(224,'50','/r/audi/m2866/',1083),(225,'5000','/r/audi/m2975/',1083),(226,'80','/r/audi/m35/',1083),(227,'90','/r/audi/m36/',1083),(228,'A1','/r/audi/m1931/',1083),(229,'A1 Sportback','/r/audi/m2281/',1083),(230,'A2','/r/audi/m16/',1083),(231,'A3','/r/audi/m17/',1083),(232,'A3 Cabriolet','/r/audi/m1879/',1083),(233,'A3 Sedan','/r/audi/m2545/',1083),(234,'A3 Sportback','/r/audi/m2395/',1083),(235,'A4','/r/audi/m19/',1083),(236,'A4 Allroad','/r/audi/m1905/',1083),(237,'A4 Avant','/r/audi/m2634/',1083),(238,'A4 Cabriolet','/r/audi/m20/',1083),(239,'A5','/r/audi/m23/',1083),(240,'A5 Cabriolet','/r/audi/m1818/',1083),(241,'A5 Sportback','/r/audi/m1801/',1083),(242,'A6','/r/audi/m25/',1083),(243,'A6 Avant','/r/audi/m2635/',1083),(244,'A7 Sportback','/r/audi/m1937/',1083),(245,'A8','/r/audi/m29/',1083),(246,'Allroad','/r/audi/m28/',1083),(247,'Cabriolet','/r/audi/m1570/',1083),(248,'Coupe','/r/audi/m1607/',1083),(249,'Q3','/r/audi/m2122/',1083),(250,'Q5','/r/audi/m33/',1083),(251,'Q7','/r/audi/m34/',1083),(252,'Quattro','/r/audi/m1852/',1083),(253,'R8','/r/audi/m32/',1083),(254,'RS Q3','/r/audi/m2542/',1083),(255,'RS2','/r/audi/m40/',1083),(256,'RS3','/r/audi/m2145/',1083),(257,'RS4','/r/audi/m22/',1083),(258,'RS5','/r/audi/m1936/',1083),(259,'RS6','/r/audi/m27/',1083),(260,'RS7','/r/audi/m2579/',1083),(261,'S2','/r/audi/m39/',1083),(262,'S3','/r/audi/m18/',1083),(263,'S4','/r/audi/m21/',1083),(264,'S5','/r/audi/m24/',1083),(265,'S5 Sportback','/r/audi/m2030/',1083),(266,'S6','/r/audi/m26/',1083),(267,'S7','/r/audi/m2379/',1083),(268,'S8','/r/audi/m30/',1083),(269,'SQ5','/r/audi/m2626/',1083),(270,'TT','/r/audi/m31/',1083),(271,'TT RS','/r/audi/m1721/',1083),(272,'TT-S','/r/audi/m1626/',1083),(273,'V8','/r/audi/m1864/',1083),(274,'A21','/r/avia/m2917/',1086),(275,'A31','/r/avia/m2918/',1086),(276,'Century','/r/buick/m564/',1096),(277,'Electra','/r/buick/m565/',1096),(278,'Enclave','/r/buick/m566/',1096),(279,'Encore','/r/buick/m2558/',1096),(280,'Estate Wagon','/r/buick/m567/',1096),(281,'Gran Sport','/r/buick/m2567/',1096),(282,'LaCrosse','/r/buick/m568/',1096),(283,'LeSabre','/r/buick/m569/',1096),(284,'Lucerne','/r/buick/m570/',1096),(285,'Park Avenue','/r/buick/m571/',1096),(286,'Rainier','/r/buick/m572/',1096),(287,'Regal','/r/buick/m573/',1096),(288,'Rendezvouz','/r/buick/m574/',1096),(289,'Riviera','/r/buick/m575/',1096),(290,'Roadmaster','/r/buick/m576/',1096),(291,'Skyhawk','/r/buick/m2659/',1096),(292,'Skylark','/r/buick/m577/',1096),(293,'Special Riviera','/r/buick/m3037/',1096),(294,'Super','/r/buick/m2563/',1096),(295,'Verano','/r/buick/m2538/',1096),(296,'Wildcat','/r/buick/m2565/',1096);
/*!40000 ALTER TABLE `models` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-03-05 21:34:33
